Successful runs:

  1. `build-release-job`: https://gitlab.torproject.org/guest475646844/test0001/-/jobs/227712

  2. `deploy-apks-job`: https://gitlab.torproject.org/guest475646844/test0001/-/jobs/227739

  3. Git [tag](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/tree/tbb-12.0.2-build1) `tbb-12.0.2-build1`: https://gitlab.torproject.org/guest475646844/test0001/-/jobs/230391

  4. Git [tag](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/tree/tbb-12.0.2-build1) `tbb-12.0.2-build1`: https://gitlab.torproject.org/guest475646844/test0001/-/jobs/230178
