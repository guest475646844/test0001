#









clear

ln=$LINES

export LINES=10

if [ "$1" == '-v' ]; then

  env | egrep LINES

  read tv

fi

while true; do

  pl="$( ps -eo pid,ppid,args | \
      \
    cat -n )"

  echo "$pl" | fold -40 | less -X

  echo -n 'ln to kill or q: '

  read inp

  if [ "$inp" == q ]; then exit; fi

  echo -n '9 for kill -9 or "": '

  read inp2

  sd=--debug

  if [ "$1" == '-v' ]; then

    echo "$pl" | fold -40 | less -X

    pn2="$( echo "$pl" | \
        \
      fold -40 )"

    echo "$pn2" | less -X

  fi

  for i in `seq 1 2`; do

    pn="$( echo "$pl" | \
        \
      sed 2>&1 $sd -n \
        \
      's/^ *'"$inp"'\t *\([0-9]\+\) \+.*$/\1/p' )"

    if [ "$1" == '-v' ]; then

      echo "    pn    '$pn'" | \
          \
        cat -n | fold -40 | less -X

    fi

    sd=

  done

  kf=

  if [ "$inp2" == 9 ]; then kf=-9; fi

  if [ "$pn" != '' ]; then

    kill 2>&1 $kf "$pn" | fold -40

  fi

done

export LINES=$ln

exit













#
